/**
 * Add your components here.
 * @module components
 * @example
 * import Footer from './Footer/Footer';
 *
 * export {
 *   Footer,
 * };
 */

import HomeView from './HomeView/HomeView';
import RatingWidget from './RatingWidget/RatingWidget';
import BonoaView from './BonoaView/BonoaView';
export { HomeView, RatingWidget, BonoaView };
