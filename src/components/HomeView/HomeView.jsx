/**
 * Faq view.
 * @module components/HomeView/HomeView
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FormattedMessage } from 'react-intl';
import { Container } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { getBonoak } from 'bonoak-views/src/actions';

/**
 * HomeView class.
 * @class HomeView
 * @extends Component
 */
class HomeView extends Component {
  /**
   * Property types.
   * @property {Object} propTypes Property types.
   * @static
   */
  static propTypes = {
    getBonoak: PropTypes.func.isRequired,
    items: PropTypes.arrayOf(
      PropTypes.shape({
        '@id': PropTypes.string,
        '@type': PropTypes.string,
        title: PropTypes.string,
        description: PropTypes.string,
        sessions: PropTypes.number,
      }),
    ),
  };

  /**
   * Default properties.
   * @property {Object} defaultProps Default properties.
   * @static
   */
  static defaultProps = {
    items: [],
  };

  /**
   * Component will mount
   * @method componentWillMount
   * @returns {undefined}
   */
  componentWillMount() {
    this.props.getBonoak();
  }

  /**
   * Render method.
   * @method render
   * @returns {string} Markup for the component.
   */
  render() {
    return (
      <Container id="home-view">
        <div className="container">
          <article id="content">
            <header>
              <h1 className="documentFirstHeading">Bonoak</h1>
            </header>
            <section id="content-core">
              {this.props.items.map(item => (
                <article className="tileItem" key={item['@id']}>
                  <h2>
                  <Link to={item['@id']} title={item['@type']}>
                    {item.title}
                  </Link>
                </h2>
                  {item.description && (
                    <div className="tileBody">
                      <span className="description">{item.description}</span>
                    </div>
                  )}
                  {item.sessions && (
                    <dl>
                        <dt>Sesio kopurua:</dt>
                        <dd>{item.sessions}</dd>
                    </dl>
                  )}
                  <div className="visualClear" />
                </article>
              ))}
            </section>
          </article>
        </div>
      </Container>
    );
  }
}

export default connect(
  state => ({
    items: state.bonoak2.items,
  }),
  dispatch => bindActionCreators({ getBonoak }, dispatch),
)(HomeView);