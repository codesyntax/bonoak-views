import React from 'react';
import { Link } from 'react-router-dom';
import { Image } from 'semantic-ui-react';

const UserCard = props => {
    const { item } = props
    return (
        <div class="card">
            <div class="content">
              {item.croppedimage && (
                <Image
                  floated="right"
                  alt={item.title}
                  src="/images/avatar/large/elliot.jpg"
                />
              )}
              <div class="header">
              {item.title}
              </div>
              <div class="description">
              {item.description}   
              </div>
            </div>
            <div class="extra content">
                <div class="ui two buttons">
                    <div class="ui basic green button">Approve</div>
                    <div class="ui basic red button">Decline</div>
                </div>
            </div>
        </div>
    )
}
export default UserCard;
