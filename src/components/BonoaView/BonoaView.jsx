/**
 * BonoaView view component.
 * @module components/theme/View/BonoaView
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Container, Image } from 'semantic-ui-react';

import { flattenToAppURL, flattenHTMLToAppURL } from '@plone/volto/helpers';

/**
 * BonoaView view component class.
 * @function BonoaView
 * @params {object} content Content object.
 * @returns {string} Markup of the component.
 */
const BonoaView = ({ content }) => (
  <Container className="view-wrapper">
    {content.title && (
      <h1 className="documentFirstHeading">
        {content.title}
        {content.subtitle && ` - ${content.subtitle}`}
      </h1>
    )}
    {content.description && (
      <p className="documentDescription">{content.description}</p>
    )}
    {content.sessions && (
      <dl>
          <dt>Sesio kopurua:</dt>
          <dd>{content.sessions}</dd>
      </dl>
    )}
    
  </Container>
);

/**
 * Property types.
 * @property {Object} propTypes Property types.
 * @static
 */
BonoaView.propTypes = {
  content: PropTypes.shape({
    title: PropTypes.string,
    description: PropTypes.string,
    sessions: PropTypes.string,
  }).isRequired,
};

export default BonoaView;
