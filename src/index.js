import { HomeView, RatingWidget, BonoaView } from 'bonoak-views/src/components';
export default function applyConfig(config) {
    config.views = {
        ...config.defaultViews,
        layoutViews: {
          ...config.layoutViews,
          home_view: HomeView,
          bonoa_view: BonoaView,
        },
      };
      
      config.widgets = {
        ...config.defaultWidgets,
        id: {
          ...config.id,
          sessions: RatingWidget,
        },
      };
    return config;
  }