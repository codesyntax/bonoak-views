/**
 * Add your actions here.
 * @module actions
 * @example
 * import {
 *   searchContent,
 * } from './search/search';
 *
 * export {
 *   searchContent,
 * };
 */

import { getBonoak } from 'bonoak-views/src/actions/bonoak/bonoak';

export { getBonoak };
