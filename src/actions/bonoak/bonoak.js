/**
 * Faq actions.
 * @module actions/bonoak/bonoak
 */

import { GET_BONOAK } from 'bonoak-views/src/constants/ActionTypes';

/**
 * Get FAQ items.
 * @function getBonoak
 * @returns {Object} Bonoak action.
 */
export function getBonoak() {
  return {
    type: GET_BONOAK,
    request: {
      op: 'get',
      path: `/@search?portal_type=Bonoa&fullobjects=true`,
    },
  };
}